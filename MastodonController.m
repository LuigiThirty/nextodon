#import "MastodonController.h"

@implementation MastodonController

- (void)awakeFromNib
{
    NSLog(@"Nextodon is awake.");
    token = [[NSString alloc] initWithCString:ACCESS_TOKEN];

    NSLog(@"Initializing JSON parser.");
    jsmn_init(&parser);
}

// Opens a socket to Mastodon via the SSL-stripping proxy.
// Returns the socket's fd.
-(int)openMastodonSocket
{
    int sockfd, portno;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    portno = 8888;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0)
      {
        NSLog(@"Error opening socket!");
        return 0;
      }

    server = gethostbyname("192.168.1.20");
    if(server == NULL)
      {
        NSLog(@"Error connecting to web proxy - is it running?");
        return 0;
      }

    bzero((char *)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
          (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);
    serv_addr.sin_port = htons(portno);

    if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
          {
            NSLog(@"Error connecting to Mastodon via proxy");
            exit(0);
          }

    return sockfd;
}

- (NSString *)fetchTimelineFromServer:(NSString *)accessToken
{
    int i, n, total_bytes_read = 0;
    NSString *timeline;
    NSMutableData *timelineJson;
	
    //NSMutableData *buffer = [NSMutableData dataWithLength:65536];
    
    // TODO: upgrade to Obj-C types - this is for interacting with BSD calls
    char recv_buffer[65536];
    int socketfd = [self openMastodonSocket];

    httpGet = [NSString stringWithFormat:@"GET /api/v1/accounts/64621/statuses HTTP/1.1\r\nHost: cybre.space\r\nAuthorization: Bearer %@\r\n\r\n", accessToken];

    NSLog(httpGet);
    
    n = write(socketfd, [httpGet cString], [httpGet cStringLength]);
    NSLog(@"Sent %d bytes", n);

    do
      {
          n = read(socketfd, recv_buffer+total_bytes_read, 1024);
          NSLog(@"Got a response: %d bytes", n);
          total_bytes_read += n;
      }
    while(n == 1024);
    
    if(total_bytes_read == -1)
        NSLog(@"Error in readv: %d", errno);
    
    NSLog(@"Got %d bytes", total_bytes_read);

    timelineJson = [NSMutableData dataWithBytes:recv_buffer length:total_bytes_read];

    timeline = [[NSString alloc] initWithData:timelineJson encoding:NSUnicodeStringEncoding];
    return timeline;
}

- (void)fetchTimeline:(id)sender
{
    jsmntok_t *tokens;
    int r;
    NSString *timeline = [self fetchTimelineFromServer:token];
    NSData *unicode_timeline;
    int unicode_timeline_length; 
    char *cstr_timeline;

    // Everything below this line is wrong, bad, crashes
    NSLog(@"Creating C-string timeline");

    unicode_timeline = [timeline dataUsingEncoding:NSUnicodeStringEncoding allowLossyConversion:YES];
    unicode_timeline_length = [unicode_timeline length];

    if(unicode_timeline == nil)
      {
        NSLog(@"Aborting timeline fetch.");
        return;
      }
    
    cstr_timeline = calloc(1, (unicode_timeline_length+1));
    memcpy(cstr_timeline, [unicode_timeline bytes], unicode_timeline_length);

    NSLog(@"****");
    NSLog(@"%@", cstr_timeline);
    NSLog(@"****");
    
    r = jsmn_parse(&parser, cstr_timeline, strlen(cstr_timeline), NULL, 256);
    if(r > 0)
      {
        NSLog(@"String contains %d tokens", r);
      }
    else
      {
        NSLog(@"Jasmine error: %d", r);
      }

    //NSLog(timeline);
}

@end
