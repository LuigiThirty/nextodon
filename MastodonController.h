#import <AppKit/AppKit.h>
#import <Foundation/Foundation.h>

#import <errno.h>
#import <unistd.h>
#import <stdio.h>
#import <stdlib.h>

#import <sys/socket.h>
#import <sys/types.h>
#import <sys/uio.h>

#import <netdb.h>
#import <netinet/in.h>

#import "jsmn.h"

int read(int fd, void *buf, size_t count);
int write(int fd, const void *buf, size_t count);

const char ACCESS_TOKEN[] = "mastodon_oauth_access_token_goes_here";

@interface MastodonController : NSObject
{
    NSString *token;
    NSString *httpGet;

    jsmn_parser parser;
    
    id timelineText;		
}
- (void)fetchTimeline:(id)sender;
- (NSString *)fetchTimelineFromServer:(NSString *)accessToken;
- (int)openMastodonSocket;

@end
